package StackTrekActivity.E16dot1.Controller;

import StackTrekActivity.E16dot1.Repository.Course;
import StackTrekActivity.E16dot1.Service.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/course")
public class CourseController {
    @Autowired
    private CourseService courseService;

    @PostMapping("/addCourse")
    public Course addCourse(@RequestBody Course course){
        return courseService.save(course);
    }

    @GetMapping("/getAllCourses")
    public List<Course> getAllCourses(){
        return courseService.findAll();
    }

    @GetMapping("/getCourseById")
    public Course getCourseById(@RequestParam long id){
        return courseService.findById(id);
    }

    @DeleteMapping("/deleteCourse/{id}")
    public void deleteCourse(@PathVariable long id){
        courseService.deleteById(id);
    }

    @PutMapping("/updateCourse/{id}")
    public Course updateCourse(@PathVariable long id, @RequestBody Course course){
        return courseService.updateById(id, course);
    }

}
