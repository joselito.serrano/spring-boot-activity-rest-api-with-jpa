package StackTrekActivity.E16dot1.Service;

import StackTrekActivity.E16dot1.Exception.BadRequestException;
import StackTrekActivity.E16dot1.Repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class Student_CourseServiceImplementation implements Student_CourseService {

    @Autowired
    Student_CourseRepository studentCourseRepository;

    @Autowired
    StudentRepository studentRepository;


    @Override
    public Student_Course save(Student_Course studentCourse) {
        return studentCourseRepository.save(studentCourse);
    }

    @Override
    public List<Student_Course> findAll() {
        return studentCourseRepository.findAll();
    }

    @Override
    public Student_Course findById(long id) {
        return studentCourseRepository.findById(id).orElseThrow(() -> new BadRequestException("Student_Course does not exist"));
    }

    @Override
    public void deleteById(long id) {
        studentCourseRepository.deleteById(id);
    }

    @Override
    public Student_Course updateById(long id, Student_Course studentCourse) {
        studentCourse.setId(id);
        return studentCourseRepository.save(studentCourse);
    }

    public void deleteByStudenAndCourse(long studentId, Course course) {
        Student student = studentRepository.findById(studentId)
                .orElseThrow(() -> new BadRequestException("Student with id " + studentId + " not found"));

        Student_Course studentCourse = studentCourseRepository.findByStudentAndCourse(student, course)
                .orElseThrow(() -> new BadRequestException("Student is not enrolled in this course"));

        studentCourseRepository.delete(studentCourse);
    }
}
