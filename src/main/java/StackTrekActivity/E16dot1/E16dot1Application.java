package StackTrekActivity.E16dot1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class E16dot1Application {

	public static void main(String[] args) {
		SpringApplication.run(E16dot1Application.class, args);
	}

}
