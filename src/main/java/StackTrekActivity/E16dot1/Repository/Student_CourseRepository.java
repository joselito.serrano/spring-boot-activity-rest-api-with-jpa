package StackTrekActivity.E16dot1.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface Student_CourseRepository extends JpaRepository<Student_Course, Long> {
    Optional<Student_Course> findByStudentAndCourse(Student student, Course course);
}
