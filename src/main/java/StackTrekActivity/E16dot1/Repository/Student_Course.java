package StackTrekActivity.E16dot1.Repository;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = {"student_id", "course_id"}, name = "UK_Student_Course")})
@Setter
@Getter
public class Student_Course {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne
    @JoinColumn(name = "student_id"
            , referencedColumnName = "Id"
            , foreignKey = @ForeignKey(name = "FK_Student"
            , value = ConstraintMode.PROVIDER_DEFAULT))
    private Student student;

    @ManyToOne
    @JoinColumn(name = "course_id"
            , referencedColumnName = "Id"
            , foreignKey = @ForeignKey(name = "FK_Course"
            , value = ConstraintMode.PROVIDER_DEFAULT))
    private Course course;
}
