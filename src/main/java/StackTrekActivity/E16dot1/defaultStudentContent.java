package StackTrekActivity.E16dot1;

import StackTrekActivity.E16dot1.Repository.Course;
import StackTrekActivity.E16dot1.Repository.Student;
import StackTrekActivity.E16dot1.Repository.Student_Course;
import StackTrekActivity.E16dot1.Service.CourseService;
import StackTrekActivity.E16dot1.Service.StudentService;
import StackTrekActivity.E16dot1.Service.Student_CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component
public class defaultStudentContent implements CommandLineRunner {
    @Autowired
    private StudentService studentService;

    @Autowired
    private CourseService courseService;

    @Autowired
    private Student_CourseService studentCourseService;

    private final ArrayList<Course> defaultCourseData = new ArrayList<>();
    private final ArrayList<Student> defaultStudentData = new ArrayList<>();

    @Override
    public void run(String... args) throws Exception {
        populateCourseTable();
        populateStudentTable();
        populateStudentCourseTable();
    }

    //To Refactor
    private void populateCourseTable() {
        Course course1 = new Course();
        course1.setName("Web Development");
        course1.setDescription("Learn how to build dynamic and responsive websites using HTML, CSS, JavaScript and various web development frameworks.");
        courseService.save(course1);
        defaultCourseData.add(course1);

        Course course2 = new Course();
        course2.setName("Mobile Application Development");
        course2.setDescription("This course teaches students how to design and develop mobile applications for Android and iOS devices.");
        courseService.save(course2);
        defaultCourseData.add(course2);

        Course course3 = new Course();
        course3.setName("Data Science");
        course3.setDescription("Learn the techniques and tools used to analyze and interpret complex data sets and make data-driven decisions.");
        courseService.save(course3);
        defaultCourseData.add(course3);

        Course course4 = new Course();
        course4.setName("Digital Marketing");
        course4.setDescription("This course covers the principles and strategies of digital marketing, including SEO, SEM, social media marketing, and email marketing.");
        courseService.save(course4);
        defaultCourseData.add(course4);

        Course course5 = new Course();
        course5.setName("Cybersecurity");
        course5.setDescription("Learn how to protect computer systems and networks from unauthorized access, attacks, and data breaches.");
        courseService.save(course5);
        defaultCourseData.add(course5);

        Course course6 = new Course();
        course6.setName("Graphic Design");
        course6.setDescription("This course teaches students the principles of graphic design, including typography, color theory, and layout, and how to use design software.");
        courseService.save(course6);
        defaultCourseData.add(course6);

        Course course7 = new Course();
        course7.setName("Business Administration");
        course7.setDescription("Learn the fundamentals of business management, including accounting, finance, marketing, and operations.");
        courseService.save(course7);
        defaultCourseData.add(course7);

        Course course8 = new Course();
        course8.setName("Human Resources Management");
        course8.setDescription("This course covers the principles and practices of human resources management, including recruitment, employee relations, and compensation and benefits.");
        courseService.save(course8);
        defaultCourseData.add(course8);

        Course course9 = new Course();
        course9.setName("Environmental Science");
        course9.setDescription("Learn about environmental issues, including climate change, pollution, and biodiversity, and how to address them through sustainable practices.");
        courseService.save(course9);
        defaultCourseData.add(course9);

        Course course10 = new Course();
        course10.setName("Artificial Intelligence and Machine Learning");
        course10.setDescription("This course covers the theory and practice of AI and machine learning, including neural networks, deep learning, and natural language processing.");
        courseService.save(course10);
        defaultCourseData.add(course10);
    }

    private void populateStudentTable() {
        Student student1 = new Student();
        student1.setFirstName("Michael");
        student1.setLastName("Smith");
        student1.setEmail("michaelsmith@gmail.com");
        studentService.save(student1);

        Student student2 = new Student();
        student2.setFirstName("Jessica");
        student2.setLastName("Johnson");
        student2.setEmail("jessicajohnson@hotmail.com");
        studentService.save(student2);

        Student student3 = new Student();
        student3.setFirstName("William");
        student3.setLastName("Brown");
        student3.setEmail("williambrown@yahoo.com");
        studentService.save(student3);

        Student student4 = new Student();
        student4.setFirstName("Olivia");
        student4.setLastName("Davis");
        student4.setEmail("oliviadavis@outlook.com");
        studentService.save(student4);

        Student student5 = new Student();
        student5.setFirstName("Christopher");
        student5.setLastName("Garcia");
        student5.setEmail("christophergarcia@aol.com");
        studentService.save(student5);

        Student student6 = new Student();
        student6.setFirstName("Sophia");
        student6.setLastName("Rodriguez");
        student6.setEmail("sophiarodriguez@protonmail.com");
        studentService.save(student6);

        Student student7 = new Student();
        student7.setFirstName("Daniel");
        student7.setLastName("Martinez");
        student7.setEmail("danielmartinez@icloud.com");
        studentService.save(student7);

        Student student8 = new Student();
        student8.setFirstName("Emily");
        student8.setLastName("Gonzalez");
        student8.setEmail("emilygonzalez@zoho.com");
        studentService.save(student8);

        Student student9 = new Student();
        student9.setFirstName("Matthew");
        student9.setLastName("Wilson");
        student9.setEmail("matthewwilson@mail.com");
        studentService.save(student9);

        Student student10 = new Student();
        student10.setFirstName("Madison");
        student10.setLastName("Lopez");
        student10.setEmail("madisonlopez@gmx.com");
        studentService.save(student10);

        defaultStudentData.addAll(studentService.findAll());
    }

    private void populateStudentCourseTable() {
        Student_Course studentCourse1 = new Student_Course();
        studentCourse1.setStudent(defaultStudentData.get(0));
        studentCourse1.setCourse(defaultCourseData.get(0));
        studentCourseService.save(studentCourse1);

        // student 2 enrolls in course 2
        Student_Course studentCourse2 = new Student_Course();
        studentCourse2.setStudent(defaultStudentData.get(1));
        studentCourse2.setCourse(defaultCourseData.get(1));
        studentCourseService.save(studentCourse2);

        // student 3 has no course

        // student 4 enrolls in course 1 and course 2
        Student_Course studentCourse4a = new Student_Course();
        studentCourse4a.setStudent(defaultStudentData.get(3));
        studentCourse4a.setCourse(defaultCourseData.get(0));
        studentCourseService.save(studentCourse4a);

        Student_Course studentCourse4b = new Student_Course();
        studentCourse4b.setStudent(defaultStudentData.get(3));
        studentCourse4b.setCourse(defaultCourseData.get(1));
        studentCourseService.save(studentCourse4b);

        // student 5 enrolls in course 3
        Student_Course studentCourse5 = new Student_Course();
        studentCourse5.setStudent(defaultStudentData.get(4));
        studentCourse5.setCourse(defaultCourseData.get(2));
        studentCourseService.save(studentCourse5);

        // student 6 enrolls in course 2
        Student_Course studentCourse6 = new Student_Course();
        studentCourse6.setStudent(defaultStudentData.get(5));
        studentCourse6.setCourse(defaultCourseData.get(1));
        studentCourseService.save(studentCourse6);

        // student 7 has no course

        // student 8 enrolls in course 4
        Student_Course studentCourse8 = new Student_Course();
        studentCourse8.setStudent(defaultStudentData.get(7));
        studentCourse8.setCourse(defaultCourseData.get(3));
        studentCourseService.save(studentCourse8);

        // student 9 has no course

        // student 10 enrolls in course 5
        Student_Course studentCourse10 = new Student_Course();
        studentCourse10.setStudent(defaultStudentData.get(9));
        studentCourse10.setCourse(defaultCourseData.get(4));
        studentCourseService.save(studentCourse10);
    }

}
