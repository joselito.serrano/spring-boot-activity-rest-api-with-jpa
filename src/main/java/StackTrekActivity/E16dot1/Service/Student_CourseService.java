package StackTrekActivity.E16dot1.Service;


import StackTrekActivity.E16dot1.Repository.Course;
import StackTrekActivity.E16dot1.Repository.Student_Course;

import java.util.List;

public interface Student_CourseService {

    Student_Course save(Student_Course studentCourse);

    List<Student_Course> findAll();

    Student_Course findById(long id);

    void deleteById(long id);

    Student_Course updateById(long id, Student_Course studentCourse);

    void deleteByStudenAndCourse(long studentId, Course course);

}
