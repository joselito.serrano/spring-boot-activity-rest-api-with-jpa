package StackTrekActivity.E16dot1.Exception;

public class CourseStillHasStudentsException extends RuntimeException {
    public CourseStillHasStudentsException() {
        super("Cannot delete course because it still has associated students.");
    }
}
