# Instructions
## Spring Boot Activity - Rest API with JPA

Student Spec:
- Student (Id, FirstName, LastName, Email, Course)

Required Endpoints:
- addStudent
- getAllStudents
- getById
- deleteById
- updateById
- enroll(Course course)
- drop(Course course)

Courses:
- addCourse
- getAllCourses
- getCourseById
- deleteCourse
- updateCourse

Expected outputs:
- Database name: school Table: student note: should have at least 10 entries
- Use JPA
- Create Rest Controllers
- Postman requests
