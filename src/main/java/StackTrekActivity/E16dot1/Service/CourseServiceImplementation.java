package StackTrekActivity.E16dot1.Service;

import StackTrekActivity.E16dot1.Exception.BadRequestException;
import StackTrekActivity.E16dot1.Repository.Course;
import StackTrekActivity.E16dot1.Repository.CourseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CourseServiceImplementation implements CourseService {

    @Autowired
    CourseRepository courseRepository;

    @Override
    public Course save(Course course) {
        return  courseRepository.save(course);
    }

    @Override
    public List<Course> findAll() {
        return  courseRepository.findAll();
    }

    @Override
    public Course findById(long id) {
        return courseRepository.findById(id).orElseThrow(() -> new BadRequestException("Course does not exist"));
    }

    @Override
    public void deleteById(long id) {
        courseRepository.deleteById(id);
    }

    @Override
    public Course updateById(long id, Course course) {
        course.setId(id);
        return courseRepository.save(course);
    }
}
