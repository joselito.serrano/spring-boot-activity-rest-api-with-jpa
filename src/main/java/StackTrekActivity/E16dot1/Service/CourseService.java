package StackTrekActivity.E16dot1.Service;

import StackTrekActivity.E16dot1.Repository.Course;

import java.util.List;


public interface CourseService {

    Course save(Course course);

    List<Course> findAll();

    Course findById(long id);

    void deleteById(long id);

    Course updateById(long id, Course course);
}
