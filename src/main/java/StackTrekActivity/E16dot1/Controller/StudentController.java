package StackTrekActivity.E16dot1.Controller;

import StackTrekActivity.E16dot1.Repository.Course;
import StackTrekActivity.E16dot1.Repository.Student;
import StackTrekActivity.E16dot1.Repository.Student_Course;
import StackTrekActivity.E16dot1.Service.StudentService;
import StackTrekActivity.E16dot1.Service.Student_CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/student")
public class StudentController {

    @Autowired
    private StudentService studentService;

    @Autowired
    private Student_CourseService studentCourseService;

    @PostMapping("/addStudent")
    public Student addStudent(@RequestBody Student student){
        return studentService.save(student);
    }

    @GetMapping("/getAllStudents")
    public List<Student> getAllStudents(){
        return studentService.findAll();
    }

    @GetMapping("/getById")
    public Student getById(@RequestParam long id){
        return studentService.findById(id);
    }

    @DeleteMapping("/deleteById/{id}")
    public void deleteById(@PathVariable long id){
        studentService.deleteById(id);
    }

    @PutMapping("/updateById/{id}")
    public Student updateById(@PathVariable long id, @RequestBody Student student){
        return studentService.updateById(id, student);
    }

    @PostMapping("/enroll/{id}")
    public Student_Course enroll(@PathVariable long id, @RequestBody Course course){
        Student student = studentService.findById(id);

        Student_Course studentCourse = new Student_Course();
        studentCourse.setStudent(student);
        studentCourse.setCourse(course);
        return studentCourseService.save(studentCourse);
    }

    @DeleteMapping("/drop/{id}")
    public void drop(@PathVariable long id, @RequestBody Course course){
         studentCourseService.deleteByStudenAndCourse(id, course);
    }

}