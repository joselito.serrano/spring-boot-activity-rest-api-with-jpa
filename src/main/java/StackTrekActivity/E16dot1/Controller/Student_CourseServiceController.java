package StackTrekActivity.E16dot1.Controller;

import StackTrekActivity.E16dot1.Repository.Course;
import StackTrekActivity.E16dot1.Repository.Student_Course;
import StackTrekActivity.E16dot1.Service.Student_CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/Student_Course")
public class Student_CourseServiceController {

    @Autowired
    private Student_CourseService studentCourseService;

    @PostMapping("/enroll")
    public Student_Course enroll(@RequestBody Student_Course studentCourse) {
        return studentCourseService.save(studentCourse);
    }

    @GetMapping("/getAllEnrolled")
    public List<Student_Course> getAllEnrolled() {
        return studentCourseService.findAll();
    }
}
